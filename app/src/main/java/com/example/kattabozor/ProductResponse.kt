package com.example.kattabozor

import com.google.gson.annotations.SerializedName

data class ProductResponse(
    @SerializedName("offers")
    val offers: List<Offer>
)