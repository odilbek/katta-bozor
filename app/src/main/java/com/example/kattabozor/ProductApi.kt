package com.example.kattabozor

import com.google.android.gms.analytics.ecommerce.Product
import retrofit2.Call
import retrofit2.http.GET

interface ProductApi {
    @GET("products")
    fun getProducts(): Call<ProductResponse>
}
