package com.example.kattabozor

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

class ProductViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    private val nameTextView: TextView = itemView.findViewById(R.id.title)
    private val brandTextView: TextView = itemView.findViewById(R.id.brand)
    private val categoryTextView: TextView = itemView.findViewById(R.id.category)
    private val merchantTextView: TextView = itemView.findViewById(R.id.merchant)
    private val imageView: ImageView = itemView.findViewById(R.id.img)

    fun bind(product: Offer) {
        nameTextView.text = product.name
        brandTextView.text = product.brand
        categoryTextView.text = product.category
        merchantTextView.text = product.merchant
        Glide.with(itemView.context)
            .load(product.image.url)
            .into(imageView)
    }
}
