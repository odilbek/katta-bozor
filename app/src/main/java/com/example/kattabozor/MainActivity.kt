package com.example.kattabozor

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: ProductAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recyclerView = findViewById(R.id.recycler_view)
        recyclerView.layoutManager = LinearLayoutManager(this)

        adapter = ProductAdapter()
        recyclerView.adapter = adapter

        loadProducts()

         fetchProductData()
    }

    private fun loadProducts() {
        val retrofit = Retrofit.Builder()
            .baseUrl("https://www.kattabozor.uz/hh/test/api/v1/")
            .addConverterFactory(GsonConverterFactory.create())
            .client(OkHttpClient())
            .build()

        val api = retrofit.create(ProductApi::class.java)

        api.getProducts().enqueue(object : Callback<ProductResponse> {
            override fun onResponse(call: Call<ProductResponse>, response: Response<ProductResponse>) {
                if (response.isSuccessful) {
                    val products = response.body()?.offers ?: listOf()
                    adapter.setData(products)
                }
            }

            override fun onFailure(call: Call<ProductResponse>, t: Throwable) {
                Log.e("ProductList", "Failed to get products", t)
            }
        })
    }

    private fun fetchProductData() {
        val url = "https://www.kattabozor.uz/hh/test/api/v1/offers"
        val request = JsonObjectRequest(Request.Method.GET, url, null,
            { response ->
                val product = Gson().fromJson(response.toString(), ProductResponse::class.java)
                adapter.setData(product.offers)
            },
            { error ->
                Log.e("MainActivity", "Error fetching product data: ${error.message}")
            })

        VolleySingleton.getInstance(this).addToRequestQueue(request)
    }
}









