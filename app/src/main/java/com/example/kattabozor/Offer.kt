package com.example.kattabozor

import com.google.gson.annotations.SerializedName

data class Offer(
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("brand")
    val brand: String,
    @SerializedName("merchant")
    val merchant: String,
    @SerializedName("category")
    val category: String,
    @SerializedName("image")
    val image: Image,
    @SerializedName("attributes")
    val attributes: List<Attribute>
)